//
//  ViewController.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

protocol TableActionProtocol {
    func cellDidSelect(object: CurrencyInformation, bankType: BankMode)
}

final class ViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet var privateTableView: PrivateTableView!
    @IBOutlet var nbuTableView: PrivateTableView!
    @IBOutlet var privateTitleView: UIView!
    @IBOutlet var nbuTitleView: UIView!
    @IBOutlet var privateDateLabel: UILabel!
    @IBOutlet var NBUDateLabel: UILabel!
    @IBOutlet var loaderForPrivate: UIActivityIndicatorView!
    @IBOutlet var loaderForNBU: UIActivityIndicatorView!

    //MARK: - Properties
    private var networkService = NetworkService()
    private var datePickerBankMode: BankMode = .privat
    let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    func config() {

        dateFormatter.dateFormat = "dd.MM.yyy"

        privateDateLabel.text = dateFormatter.string(from: Date())
        NBUDateLabel.text = dateFormatter.string(from: Date())

        privateTableView.register(R.nib.privateTableViewCell)
        let privateTableModel = PrivateTableModel()
        privateTableModel.tableDelegate = privateTableView
        privateTableView.model = privateTableModel
        privateTableView.dataSource = privateTableView
        privateTableView.delegate = privateTableView
        privateTableView.separatorStyle = .none
        privateTableView.mode = .privat
        privateTableView.delegateAction = self

        nbuTableView.register(R.nib.nbuTableViewCell)
        let nbuTableModel = PrivateTableModel()
        nbuTableModel.tableDelegate = nbuTableView
        nbuTableView.model = nbuTableModel
        nbuTableView.dataSource = nbuTableView
        nbuTableView.delegate = nbuTableView
        nbuTableView.separatorStyle = .none
        nbuTableView.mode = .nbu
        nbuTableView.delegateAction = self

        getNewCurrencyForBoth(date: Date())

        let gesturePrivate = UITapGestureRecognizer(target: self, action:  #selector(self.tapPrivateDate))
        self.privateTitleView.addGestureRecognizer(gesturePrivate)

        let gestureNBU = UITapGestureRecognizer(target: self, action:  #selector(self.tapNBUDate))
        self.nbuTitleView.addGestureRecognizer(gestureNBU)
    }

}

extension ViewController {

    func getNewCurrencyForBoth(date: Date) {
        loaderForPrivate.startAnimating()
        loaderForNBU.startAnimating()
        networkService.getCurrentCurrency(date: date) { [weak self] (currency) in
            self?.privateTableView.model.updateModel(currencyArray: currency)
            self?.nbuTableView.model.updateModel(currencyArray: currency)
            self?.loaderForNBU.stopAnimating()
            self?.loaderForPrivate.stopAnimating()
        }
    }
}

//MARK: - TableActionProtocol
extension ViewController: TableActionProtocol {

    func cellDidSelect(object: CurrencyInformation, bankType: BankMode) {
        switch bankType {
        case .nbu:
            privateTableView.selectCell(with: object)
        case .privat:
            nbuTableView.selectCell(with: object)
        }
    }

    func getNewCurrency(date: Date, modelOwner: PrivateTableModelProtocol) {

        switch datePickerBankMode {
        case .privat:
            loaderForPrivate.startAnimating()
        case .nbu:
            loaderForNBU.startAnimating()
        }

        networkService.getCurrentCurrency(date: date) { [weak self] (currency) in
            modelOwner.updateModel(currencyArray: currency)
            self?.loaderForPrivate.stopAnimating()
            self?.loaderForNBU.stopAnimating()
        }
    }
}

//MARK: - Tap Action
extension ViewController {

    @objc func tapPrivateDate(_ sender: UITapGestureRecognizer){
        datePickerBankMode = .privat

        let controller = UIViewController()
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.locale = Locale.current
        picker.maximumDate = Date()

        if let dateString = privateDateLabel.text {
            if let date = dateFormatter.date(from: dateString) {
                picker.date = date
            }
        }

        controller.view.addSubview(picker)

        controller.modalPresentationStyle = .popover
        controller.preferredContentSize = CGSize(width: 350, height: 200)

        let popoverPresentationController = controller.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.sourceView = privateTitleView
        popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: privateTitleView.frame.size.width, height: privateTitleView.frame.size.height)

        self.present(controller, animated: true)
    }

    @objc func tapNBUDate(_ sender: UITapGestureRecognizer){

        datePickerBankMode = .nbu
        let controller = UIViewController()
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.locale = Locale.current
        picker.maximumDate = Date()

        if let dateString = NBUDateLabel.text {
            if let date = dateFormatter.date(from: dateString) {
                picker.date = date
            }
        }

        controller.view.addSubview(picker)

        controller.modalPresentationStyle = .popover
        controller.preferredContentSize = CGSize(width: 350, height: 200)

        let popoverPresentationController = controller.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.sourceView = nbuTitleView
        popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: nbuTitleView.frame.size.width, height: nbuTitleView.frame.size.height)

        self.present(controller, animated: true)
    }

}

//MARK: - UIPopoverPresentationControllerDelegate
extension ViewController: UIPopoverPresentationControllerDelegate {

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyy"

        let viewController = popoverPresentationController.presentedViewController
        let datePicker = viewController.view.subviews.first as!  UIDatePicker

        switch datePickerBankMode {
        case .privat:
            getNewCurrency(date: datePicker.date, modelOwner: privateTableView.model)
            privateDateLabel.text  = dateFormatter.string(from: datePicker.date)
        case .nbu:
            getNewCurrency(date: datePicker.date, modelOwner: nbuTableView.model)
            NBUDateLabel.text  = dateFormatter.string(from: datePicker.date)
        }
    }
}

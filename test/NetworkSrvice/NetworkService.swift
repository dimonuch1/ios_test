//
//  NetworkService.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

final class NetworkService {

    private let urlString = "https://api.privatbank.ua/p24api/exchange_rates?json&date="

    func getCurrentCurrency(date: Date,completion: @escaping ([CurrencyInformation]) -> Void) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyy"
        let dateString = dateFormatter.string(from: date)

        let currentUrlString  = urlString + dateString

        Alamofire.request(currentUrlString).responseJSON { response in

            if let json = response.result.value as? [String: Any] {

                let array = json["exchangeRate"] as! Array<[String: Any]>

                var arrayCurrency = [CurrencyInformation]()

                for (index, value) in array.enumerated() {
                    if index != 0 {
                        if let currencyInfo = CurrencyInformation(with: value) {
                            arrayCurrency.append(currencyInfo)
                        }
                    }
                }
                completion(arrayCurrency)
            }
        }

    }
}

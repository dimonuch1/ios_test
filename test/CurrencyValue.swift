//
//  CurrencyValue.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import Foundation

struct CurrencyInformation {
    var currency: CurrencyValue
    var saleRateNB: Double?
    var purchaseRateNB: Double?
    var saleRate: Double?
    var purchaseRate: Double?

    enum CodingKeys: String {
        case currency
        case saleRateNB
        case purchaseRateNB
        case saleRate
        case purchaseRate
        case exchangeRate
    }

    init?(with json: [String: Any]) {

        guard let currency = json[CodingKeys.currency.rawValue] as? String , let currencyValue = CurrencyValue(rawValue: currency) else {
            return nil
        }

        self.currency = currencyValue

        if let saleRateNB = json[CodingKeys.saleRateNB.rawValue] as? Double {
            self.saleRateNB = saleRateNB
        } else {
            self.saleRateNB = nil
        }

        if let purchaseRateNB = json[CodingKeys.purchaseRateNB.rawValue] as? Double {
            self.purchaseRateNB = purchaseRateNB
        } else {
            self.purchaseRateNB = nil
        }

        if let saleRate = json[CodingKeys.saleRate.rawValue] as? Double {
            self.saleRate = saleRate
        } else {
            self.saleRate = nil
        }

        if let purchaseRate = json[CodingKeys.purchaseRate.rawValue] as? Double {
            self.purchaseRate = purchaseRate
        } else {
            self.purchaseRate = nil
        }
    }
}

enum CurrencyValue: String {
    case USD
    case RUB
    case EUR
    case CAD
    case CNY
    case CZK
    case DKK
    case HUF
    case ILS
    case JPY
    case KZT
    case MDL
    case NOK
    case SGD
    case SEK
    case CHF
    case GBP
    case UZS
    case BYN
    case TMT
    case AZN
    case TRY
    case GEL
    case PLZ

    func getRussanText() -> String {
        switch self {
        case .USD:
            return "Доллар США"
        case .RUB:
            return "Росийский рубль"
        case .EUR:
            return "Евро"
        case .CAD:
            return "Валюта CAD"
        case .CNY:
            return "Валюта CNY"
        case .CZK:
            return "Валюта CZK"
        case .DKK:
            return "Валюта DKK"
        case .HUF:
            return "Валюта HUF"
        case .ILS:
            return "Валюта ILS"
        case .JPY:
            return "Валюта JPY"
        case .KZT:
            return "Валюта KZT"
        case .MDL:
            return "Валюта MDL"
        case .NOK:
            return "Валюта NOK"
        case .SGD:
            return "Валюта SGD"
        case .SEK:
            return "Валюта SEK"
        case .CHF:
            return "Валюта CHF"
        case .GBP:
            return "Валюта GBP"
        case .UZS:
            return "Валюта UZS"
        case .BYN:
            return "Валюта BYN"
        case .TMT:
            return "Валюта TMT"
        case .AZN:
            return "Валюта AZN"
        case .TRY:
            return "Валюта TRY"
        case .GEL:
            return "Валюта GEL"
        case .PLZ:
            return "Валюта PLZ"
        }
    }

}

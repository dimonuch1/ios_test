//
//  PrivateTableViewCell.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

final class PrivateTableViewCell: UITableViewCell {

    //MARK: - IBOutelt
    @IBOutlet var currencyNameLabel: UILabel!
    @IBOutlet var purchaseLabel: UILabel!
    @IBOutlet var saleLabel: UILabel!

    override func prepareForReuse() {
        currencyNameLabel.text = ""
        purchaseLabel.text = ""
        saleLabel.text = ""
    }
}

//MARK: - Config
extension PrivateTableViewCell {

    func config(data: CurrencyInformation) {
        currencyNameLabel.text = data.currency.getRussanText()

        if let purchase = data.purchaseRate {
            purchaseLabel.text = String(format:"%.3f", purchase)
        } else {
            purchaseLabel.text = "-"
        }

        if let sale = data.saleRate {
            saleLabel.text = String(format:"%.3f", sale)
        } else {
            saleLabel.text = "-"
        }

    }
}

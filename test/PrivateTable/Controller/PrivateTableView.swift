//
//  PrivateTableView.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

enum BankMode: String {
    case privat
    case nbu
}

final class PrivateTableView: UITableView {

    weak var delegateAction: ViewController!
    var model: PrivateTableModelProtocol!
    var mode: BankMode = .privat

    private var cellHeight: CGFloat {
        switch mode {
        case .privat:
            return 65
        case .nbu:
            return 50
        }
    }

}

//MARK: - UITableViewDataSource
extension PrivateTableView: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.amountElements()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch mode {
        case .privat:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.privateTableViewCell, for: indexPath)!
            let object = model.object(at: indexPath)
            cell.config(data: object)
            return cell
        case .nbu:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.nbuTableViewCell, for: indexPath)!
            let object = model.object(at: indexPath)
            cell.config(data: object, indexPath: indexPath)
            return cell
        }

    }

}

//MARK: - UITableViewDelegate
extension PrivateTableView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = model.object(at: indexPath)
        delegateAction.cellDidSelect(object: object, bankType: mode)
    }
}

//MARK: - Select cell
extension PrivateTableView {

    func selectCell(with object: CurrencyInformation) {
        let selectingIndex = model.getIndexPath(by: object)
        self.selectRow(at: selectingIndex, animated: true, scrollPosition: .middle)
    }
}

//
//  PrivateTableModel.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import Foundation

protocol PrivateTableModelProtocol {
    func amountElements() -> Int
    func updateModel(currencyArray: [CurrencyInformation])
    func object(at indexPath: IndexPath) -> CurrencyInformation
    func getIndexPath(by object: CurrencyInformation) -> IndexPath
}

 final class PrivateTableModel {

    weak var tableDelegate: PrivateTableView!

    private var currencyArray = [CurrencyInformation]() {
        didSet {
            tableDelegate.reloadData()
        }
    }
}

//MARK: - PrivateTableModelProtocol
extension PrivateTableModel: PrivateTableModelProtocol {
    
    func updateModel(currencyArray: [CurrencyInformation]) {
        self.currencyArray = sortedArrayForPrivateTable(array: currencyArray)
    }

    func amountElements() -> Int {
        return currencyArray.count
    }

    func object(at indexPath: IndexPath) -> CurrencyInformation {
        return currencyArray[indexPath.row]
    }

    func getIndexPath(by object: CurrencyInformation) -> IndexPath {
        for (index, value) in currencyArray.enumerated() {
            if value.currency == object.currency {
                return IndexPath(row: index, section: 0)
            }
        }
        return IndexPath(row: 0, section: 0)
    }

}

//MARK: - Helper Methods
extension PrivateTableModel {

    func sortedArrayForPrivateTable(array: [CurrencyInformation]) -> [CurrencyInformation] {
        var sortedArray = array
        for (index, value) in sortedArray.enumerated() {
            if value.currency == .EUR {
                sortedArray.swapAt(0, index)
            }

            if value.currency == .USD {
                sortedArray.swapAt(1, index)
            }

            if value.currency == .RUB {
                sortedArray.swapAt(2, index)
            }
        }
        return sortedArray
    }
}

//
//  NBUTableViewCell.swift
//  test
//
//  Created by kira on 10.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

final class NBUTableViewCell: UITableViewCell {

    @IBOutlet var purchaseLabel: UILabel!
    @IBOutlet var helperLabel: UILabel!
    @IBOutlet var currencyNameLabel: UILabel!

    override func prepareForReuse() {
        purchaseLabel.text = ""
        helperLabel.text = ""
        currencyNameLabel.text = ""
        backgroundColor = .white
    }
    
}

//MARK: - Config
extension NBUTableViewCell {

    func config(data: CurrencyInformation, indexPath: IndexPath) {
        if let purchaseRateNB = data.purchaseRateNB {
            purchaseLabel.text = String(format: "%.3f\(data.currency.rawValue)", purchaseRateNB)
        }

        if indexPath.row % 2 != 0 {
            backgroundColor = UIColor(red: 207/255, green: 255/255, blue: 227/255, alpha: 1.0)
        }

        helperLabel.text = "1\(data.currency.rawValue)"
        currencyNameLabel.text = data.currency.getRussanText()
    }
}


